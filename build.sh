#!/bin/bash -ex

# CONFIG
prefix="MPEG_Streamclip"
suffix=""
munki_package_name="MPEG_Streamclip"
display_name="MPEG Streamclip"
url=`./finder.rb`

# download it (-L: follow redirects)
curl -L -o app.dmg -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/536.26.17 (KHTML, like Gecko) Version/6.0.2 Safari/536.26.17' "${url}"

# Obtain version info
# Mount disk image on temp space
# version=`defaults read "${plist}" version`
mountpoint=`hdiutil attach -mountrandom /tmp -nobrowse app.dmg | awk '/private\/tmp/ { print $3 } '`
echo Mounted on $mountpoint
mkdir "app_tmp"
mkdir "app_tmp/MPEG Streamclip"
mkdir "app_tmp/MPEG Streamclip/MPEG Streamclip.app"
mkdir "app_tmp/MPEG Streamclip/Utility MPEG2 Component M. Lion.app"
path="app_tmp/MPEG Streamclip"
ditto "${mountpoint}/MPEG Streamclip.app" "${path}/MPEG Streamclip.app"
ditto "${mountpoint}/MPEG Streamclip.app" "${path}/Utility MPEG2 Component M. Lion.app"

hdiutil detach "${mountpoint}"

hdiutil create -srcfolder app_tmp -format UDZO -o main.dmg

/usr/local/munki/makepkginfo main.dmg --item="MPEG Streamclip" --destinationpath="/Applications" > app.plist

plist=`pwd`/app.plist

/usr/local/munki/makepkginfo -f "app_tmp/MPEG Streamclip/MPEG Streamclip.app" > app1.plist
/usr/local/munki/makepkginfo -f "app_tmp/MPEG Streamclip/Utility MPEG2 Component M. Lion.app" > app2.plist

# Obtain installs array values for the applications
app1_CFBundleIdentifier=`/usr/libexec/PlistBuddy -c "print :installs:0:CFBundleIdentifier" app1.plist`
app1_CFBundleVersion=`/usr/libexec/PlistBuddy -c "print :installs:0:CFBundleVersion" app1.plist`
app1_path="/Applications/MPEG Streamclip/MPEG Streamclip.app"
app1_type="application"
app1_version_comparison_key=`/usr/libexec/PlistBuddy -c "print :installs:0:version_comparison_key" app1.plist`

app2_CFBundleIdentifier=`/usr/libexec/PlistBuddy -c "print :installs:0:CFBundleIdentifier" app2.plist`
app2_CFBundleVersion=`/usr/libexec/PlistBuddy -c "print :installs:0:CFBundleVersion" app2.plist`
app2_path="/Applications/MPEG Streamclip/Utility MPEG2 Component M. Lion.app"
app2_type="application"
app2_version_comparison_key=`/usr/libexec/PlistBuddy -c "print :installs:0:version_comparison_key" app2.plist`

version="${app1_CFBundleVersion}"

# Change path and other details in the plist
defaults write "${plist}" installer_item_location "jenkins/${prefix}-${version}${suffix}.dmg"
defaults write "${plist}" minimum_os_version "10.7.0"
defaults write "${plist}" maximum_os_version "10.14.99"
defaults write "${plist}" uninstallable -bool NO
defaults write "${plist}" name "${munki_package_name}"
defaults write "${plist}" display_name "${display_name}"
defaults write "${plist}" version "${version}"

# Create keys for the installs array
/usr/libexec/PlistBuddy -c "Add :installs:1:CFBundleIdentifier string" app.plist
/usr/libexec/PlistBuddy -c "Add :installs:1:CFBundleVersion string" app.plist
/usr/libexec/PlistBuddy -c "Add :installs:1:path string" app.plist
/usr/libexec/PlistBuddy -c "Add :installs:1:type string" app.plist
/usr/libexec/PlistBuddy -c "Add :installs:1:version_comparison_key string" app.plist

/usr/libexec/PlistBuddy -c "Add :installs:2:CFBundleIdentifier string" app.plist
/usr/libexec/PlistBuddy -c "Add :installs:2:CFBundleVersion string" app.plist
/usr/libexec/PlistBuddy -c "Add :installs:2:path string" app.plist
/usr/libexec/PlistBuddy -c "Add :installs:2:type string" app.plist
/usr/libexec/PlistBuddy -c "Add :installs:2:version_comparison_key string" app.plist

# Fill in values into the installs array
/usr/libexec/PlistBuddy -c "Set :installs:1:CFBundleIdentifier $app1_CFBundleIdentifier" app.plist
/usr/libexec/PlistBuddy -c "Set :installs:1:CFBundleVersion $app1_CFBundleVersion" app.plist
/usr/libexec/PlistBuddy -c "Set :installs:1:path $app1_path" app.plist
/usr/libexec/PlistBuddy -c "Set :installs:1:type $app1_type" app.plist
/usr/libexec/PlistBuddy -c "Set :installs:1:version_comparison_key $app1_version_comparison_key" app.plist

/usr/libexec/PlistBuddy -c "Set :installs:2:CFBundleIdentifier $app2_CFBundleIdentifier" app.plist
/usr/libexec/PlistBuddy -c "Set :installs:2:CFBundleVersion $app2_CFBundleVersion" app.plist
/usr/libexec/PlistBuddy -c "Set :installs:2:path $app2_path" app.plist
/usr/libexec/PlistBuddy -c "Set :installs:2:type $app2_type" app.plist
/usr/libexec/PlistBuddy -c "Set :installs:2:version_comparison_key $app2_version_comparison_key" app.plist

# Make readable by humans
/usr/bin/plutil -convert xml1 "${plist}"
chmod a+r "${plist}"

# Change filenames to suit
mv main.dmg   ${prefix}-${version}${suffix}.dmg
mv app.plist ${prefix}-${version}${suffix}.plist
